// standard-version-updater.js
const detectIndent = require('detect-indent')

module.exports.readVersion = function (contents) {
  return JSON.parse(contents).version;
}

module.exports.writeVersion = function (contents, version) {
  const json = JSON.parse(contents)
  json.version = version
  return JSON.stringify(json, null, detectIndent(contents).indent)
}