# Stage 1: Build the Go Application
FROM golang:1.20 AS builder

# Set the working directory inside the container
WORKDIR /app

# Copy the Go application source code into the container
COPY . .

# Build the Go application
RUN go build -o myapp

# Stage 2: Create the Final Docker Image
FROM alpine:3.14

# Copy the binary from the builder stage into the final image
COPY --from=builder /app/myapp /usr/local/bin/myapp

# Expose any necessary ports
EXPOSE 3000

# Define environment variables if needed
# ENV MY_ENV_VAR=my_value

# Set the entry point for the final image
CMD ["myapp"]