# Changelog
## [1.1.2-dev.5](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.4...v1.1.2-dev.5) (2024-05-07)

## [1.1.2-dev.4](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.2...v1.1.2-dev.4) (2024-05-07)


### 🐛 Bug Fixes

* package lock ([c915881](https://gitlab.com/rullyafrizal/go-cicd/commit/c9158813f9f54395efdd76d34c05173b04de261b))

### [1.1.2-dev.1](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.0...v1.1.2-dev.1) (2023-10-30)

### [1.1.2-dev.0](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.1-dev.1...v1.1.2-dev.0) (2023-10-30)

### [1.1.1-dev.1](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.1-dev.0...v1.1.1-dev.1) (2023-10-30)

### [1.1.1-dev.0](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.0.11-1...v1.1.1-dev.0) (2023-10-30)

### [1.0.11-1](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.0.11-0...v1.0.11-1) (2023-10-30)

### [1.0.11-0](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.0.9...v1.0.11-0) (2023-10-30)

## [1.0.10-feature/bump-version.0](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.0.9...v1.0.10-feature/bump-version.0) (2023-10-30)

### [1.0.9](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.0.8...v1.0.9) (2023-10-30)

### [1.0.8](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.0.7...v1.0.8) (2023-10-30)
